import argparse
import functools

import numpy as np
import sklearn
from sklearn.model_selection import StratifiedKFold

from explain import Explain
from explain.misc.data import CompatibilityDataSet
from explain.misc.evaluation import Evaluation

STANDARD_SCORING = {
    'f1_macro': functools.partial(sklearn.metrics.f1_score, average='macro', zero_division=0),
    'f1_micro': functools.partial(sklearn.metrics.f1_score, average='micro', zero_division=0),
    'precision_macro': functools.partial(sklearn.metrics.precision_score, average='macro', zero_division=0),
    'precision_micro': functools.partial(sklearn.metrics.precision_score, average='micro', zero_division=0),
    'recall_macro': functools.partial(sklearn.metrics.recall_score, average='macro', zero_division=0),
    'recall_micro': functools.partial(sklearn.metrics.recall_score, average='micro', zero_division=0),
}

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Demo execution of Explain.')
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--model', type=str, required=True)
    parser.add_argument('--n_jobs', type=int, required=False, default=-1)
    parser.add_argument('--verbose', type=bool, required=False, default=False)

    # destination folder
    parser.add_argument('--destination', type=str, required=False, default='')

    args = parser.parse_args()

    n_jobs = args.n_jobs
    verbose = args.verbose

    explain_model = Explain.from_json(args.model, n_jobs=n_jobs, verbose=verbose)

    evaluation = Evaluation(model=explain_model,
                            cv=StratifiedKFold(n_splits=5, shuffle=True),
                            scoring=STANDARD_SCORING,
                            n_jobs=1,
                            verbose=verbose)

    dataset = CompatibilityDataSet.load(args.dataset)
    X, y = dataset.expand()
    group_mapping = dataset.group_map

    evaluation.fit(X, y)

    print("Evaluation complete gathering collected data and saving to disk ..")
    evaluation.save_to_disk(args.destination, group_mapping=group_mapping)

    report = evaluation.report_
    print(f"Average f1 score (macro): {np.mean(report['test_f1_macro'])}")
    print(f"Average f1 score (micro): {np.mean(report['test_f1_micro'])}")
