import argparse
import functools
import os
import pickle

from sklearn.ensemble import RandomForestClassifier

from explain.base import FeatureExtraction
from explain.base.selection import mutual_information_classif, variance, anova_f, target_correlation, rfe_rank, \
    permutation_importance_score, relief_score
from explain.base.features.examples import ALL as ALL_EXAMPLE_FEATURES
from explain.misc.data import CompatibilityDataSet
from explain.misc.sampling import RandomUndersampling

MAX_250_SAMPLING = RandomUndersampling(samples_per_class=250)

METRICS = {
    'mutual_information': functools.partial(mutual_information_classif, discrete_features='auto'),
    'mutual_information_modified': functools.partial(mutual_information_classif, discrete_features='determine'),
    'variance': functools.partial(variance, normalize=None),
    'index_of_dispersion': functools.partial(variance, normalize='dispersion'),
    'anova_f': anova_f,
    'pearson_correlation': functools.partial(target_correlation, method='pearson'),
    'spearman_correlation': functools.partial(target_correlation, method='spearman'),
    'rfe_mdi': functools.partial(rfe_rank, metric='mdi', estimator=RandomForestClassifier(n_jobs=-1), n_jobs=-1),
    'rfe_pi': functools.partial(rfe_rank, metric='pi', estimator=RandomForestClassifier(n_jobs=-1), n_jobs=-1),
    'pi': functools.partial(permutation_importance_score, estimator=RandomForestClassifier(n_jobs=-1), n_jobs=-1),
    'multisurf': functools.partial(relief_score, method='multisurf', undersampling=MAX_250_SAMPLING, n_jobs=-1),
    'relieff': functools.partial(relief_score, method='relieff', undersampling=MAX_250_SAMPLING, n_jobs=-1)
}


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Demo execution of Explain\'s feature importance calculation and selection on all features.')
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--metric', choices=METRICS.keys(), required=True)
    parser.add_argument('--verbose', type=int, required=False, default=False)

    # destination folder
    parser.add_argument('--destination', type=str, required=False, default='')

    args = parser.parse_args()

    dataset = CompatibilityDataSet.load(args.dataset)
    X, y = dataset.expand()

    verbose = args.verbose

    fe = FeatureExtraction(features=ALL_EXAMPLE_FEATURES, n_jobs=-1, verbose=verbose)
    X = fe.fit(X).transform(X)

    metric = METRICS[args.metric](
        X, y,
        perform_extraction=False
    )

    with open(os.path.join(args.destination, 'feature_selection_metric.pkl'), 'wb') as metric_result_file:
        pickle.dump(metric, metric_result_file)
