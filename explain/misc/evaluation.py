import os
import pickle
import timeit

import numpy as np
import sklearn
from joblib import Parallel, delayed
from sklearn.base import BaseEstimator, clone
from sklearn.model_selection._validation import _aggregate_score_dicts
from sklearn.utils.validation import check_is_fitted

from explain.base import BaseModel


class Evaluation(BaseEstimator):
    """All in one class executing one iteration of an evaluation task.

    Parameters
    ----------
    model: BaseModel
        model used in the evaluation task
    cv: BaseCrossValidator
        cross validation strategy used to determine test and train set(s)
    scoring: dict of str to func
        mapping of name to a scoring function func(y_true, y_pred)
    n_jobs: int
        number of jobs running folds evaluations in parallel
    verbose: int
        controlling the verbosity of the parallel running evaluations

    """
    def __init__(self, model: BaseModel,
                 *,
                 cv=None,
                 scoring=None,
                 n_jobs=1,
                 verbose=False):
        self.model = model
        self.cv = cv
        self.scoring = scoring
        self.n_jobs = n_jobs
        self.verbose = verbose

    @staticmethod
    def _fit(estimator, X, y,
             *,
             scorers,
             train_idx,
             test_idx,
             fit_params):
        X_train, y_train = X[train_idx], y[train_idx]

        fit_time_start = timeit.default_timer()
        estimator.fit(X_train, y_train, **fit_params)
        fit_time = timeit.default_timer() - fit_time_start

        train_size = X_train.shape[0]
        del X_train, y_train

        X_test, y_test = X[test_idx], y[test_idx]

        pred_time_start = timeit.default_timer()
        y_pred = estimator.predict(X_test)
        pred_time = timeit.default_timer() - pred_time_start

        scores = dict()

        score_time_start = timeit.default_timer()
        for name, score_func in scorers.items():
            scores[name] = score_func(y_test, y_pred)
        confusion_matrix = sklearn.metrics.confusion_matrix(y_test, y_pred)
        score_time = timeit.default_timer() - score_time_start

        test_size = X_test.shape[0]

        del X_test, y_test

        return scores, confusion_matrix, fit_time, pred_time, score_time, train_size, test_size

    def fit(self, X, y, **fit_params):
        report = dict()

        if hasattr(X, 'shape'):
            report['n_samples'] = X.shape[0]
        else:
            report['n_samples'] = len(X)

        preprocessing_time_start = timeit.default_timer()
        X = self.model.preprocess(X, y)
        preprocessing_time = timeit.default_timer() - preprocessing_time_start

        fit_params = fit_params if fit_params is not None else {}

        parallel = Parallel(n_jobs=self.n_jobs, verbose=self.verbose)
        metrics = parallel(delayed(self._fit)(clone(self.model), X, y,
                                              scorers=self.scoring,
                                              train_idx=train_idx,
                                              test_idx=test_idx,
                                              fit_params=fit_params)
                           for train_idx, test_idx
                           in self.cv.split(X, y))

        zipped_metrics = list(zip(*metrics))

        scores, confusion_matrices, fit_times, pred_times, score_times, train_sizes, test_sizes = zipped_metrics
        scores = _aggregate_score_dicts(scores)

        report['train_size'] = np.array(train_sizes)
        report['test_size'] = np.array(test_sizes)
        report['preprocessing_time'] = preprocessing_time
        report['fit_time'] = np.array(fit_times)
        report['pred_time'] = np.array(pred_times)
        report['score_time'] = np.array(score_times)
        report['confusion_matrices'] = confusion_matrices

        for name in self.scoring:
            report['test_%s' % name] = np.array(scores[name])

        self.report_ = report
        return self

    def save_to_disk(self, result_directory,
                     *,
                     group_mapping):
        check_is_fitted(self)
        report = self.report_
        report['classes'] = group_mapping

        out_file = os.path.join(result_directory,
                                'evaluation.pkl')

        with open(out_file, 'wb') as file:
            pickle.dump(report, file)
