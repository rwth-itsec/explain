import os
import pickle
import pydoc
import random
import string
import sys
import time
import types
from pickle import Unpickler
from typing import Dict, Tuple

import numpy as np


def _compatibility_constructor(self,
                               domains,
                               groups,
                               group_map,
                               set_id=None):
    self.domains = domains
    self.groups = groups
    self.group_map = group_map
    self.id = set_id


class _CompatibilityUnpickle(Unpickler):
    def __init__(self, compatibility_mapping: Dict[str, Tuple[str, str]],
                 *args,
                 **kwargs):
        super().__init__(*args,
                         **kwargs)
        self.compatibility_mapping = compatibility_mapping

    def find_class(self, module, name):
        updated_module = module
        updated_name = name

        if name in self.compatibility_mapping:
            updated_module, updated_name = self.compatibility_mapping[name]

        return super().find_class(updated_module,
                                  updated_name)


class CompatibilityDataSet:
    """Dataset data structure holding samples with their respective target label.

    Notes
    -----
    This is a modified version of [1]_'s DataSet and can be used interchangeably in both projects.

    References
    ----------
    .. [1] S. Schuppen, D. Teubert, P. Herrmann, and U. Meyer, "FANCI:
           Feature-based Automated NXDomain Classification Intelligence"
           in USENIX Security Symposium, 2018.
    """
    def __init__(self, domains, groups, group_map, set_id=None):
        if set_id:
            self.id = set_id
        else:
            self.id = ''.join(random.choices(string.ascii_uppercase + string.digits, k=6))

        if isinstance(domains, np.ndarray):
            self.domains = domains.tolist()
        else:
            self.domains = list(domains)

        if isinstance(groups, np.ndarray):
            self.groups = groups.tolist()
        else:
            self.groups = list(groups)

        self.group_map = group_map

    def expand(self):
        return np.array(self.domains), np.array(self.groups)

    def serialize(self, where=None, keep_copy=True):
        if not where:
            set_file = 'data/' + '{!s}.pkl'.format(self.id)
        else:
            set_file = where + '/' + '{!s}.pkl'.format(self.id)

        if os.path.isfile(set_file):
            if not keep_copy:
                return
            set_file += '_' + ''.join(str(time.time()).replace('.', '_'))

        with open(set_file, 'wb') as f:
            # mock 'missing' module
            assert pydoc.locate('data_processing') is None

            dataset_mock = type("DataSet", (object,), {
                "__module__": 'data_processing',
                "__init__": _compatibility_constructor
            })

            mocked_module = types.ModuleType('data_processing')
            sys.modules['data_processing'] = mocked_module
            mocked_module.DataSet = dataset_mock

            dataset = dataset_mock(self.domains,
                                   self.groups,
                                   self.group_map,
                                   self.id)
            pickle.dump(dataset,
                        f)
            del sys.modules['data_processing']

    @staticmethod
    def load(file: str):
        with open(file, 'rb') as f:
            dataset = _CompatibilityUnpickle({
                'DataSet': ('explain.misc.data', 'CompatibilityDataSet')
            }, f).load()

        return dataset


def load_public_suffix_list(file, comments):
    """Extracts valid public suffixes from the provided file.

    Parameters
    ----------
    file: str
        filename of file containing public suffixes
    comments: list of str
        lines in the file starting with any comment token are ignored

    Returns
    -------
    public_suffix_list: list of str
        a list of unique public suffixes in lowercase

    Notes
    ------
    This functions expects one public suffix per line in the provided file.

    """
    valid_suffixes = []

    with open(file, encoding='utf-8') as suffix_list_file:
        for line in suffix_list_file:
            cleaned_line = line.strip().lower()

            if not cleaned_line:
                continue
            if cleaned_line.startswith(tuple(comments)):
                continue

            if cleaned_line.startswith('*.'):
                valid_suffixes.append(cleaned_line[2:])
            else:
                valid_suffixes.append(cleaned_line)

    # use set to increase lookup speed
    return set(valid_suffixes)
