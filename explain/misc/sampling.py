import numpy as np
import scipy as sp
import scipy.sparse
from sklearn.base import BaseEstimator
from sklearn.utils import check_random_state
from sklearn.utils.validation import check_is_fitted


class RandomUndersampling(BaseEstimator):
    """Estimator performing random undersampling of samples of majority classes.

    Parameters
    ----------
    samples_per_class: int
        classes with more than samples_per_class samples are considered majority classes
    random_state: int or instance of RandomState, optional
        seed used for random generator utilized in sampling

    Notes
    -----
    The algorithm ensures that each class has at most samples_per_class samples.

    Examples
    --------
    >>> import numpy as np
    >>> rng = np.random.RandomState(136)
    >>> X = rng.rand(10, 5)  # random sample data
    >>> y = np.array(4 * [0] + 6 * [1])
    >>> undersampling = RandomUndersampling(samples_per_class=5, random_state=rng)
    >>> X, y = undersampling.fit_resample(X, y)
    >>> _, counts = np.unique(y, return_counts=True)
    >>> print(counts)

    """
    def __init__(self, samples_per_class, random_state=None):
        self.samples_per_class = samples_per_class
        self.random_state = random_state

    def fit(self, y):
        rng = check_random_state(self.random_state)
        selected_samples = list()

        unique_labels = np.unique(y)
        for label in unique_labels:
            indexes_with_label, = np.where(y == label)
            n_samples = min(indexes_with_label.shape[0], self.samples_per_class)

            selected_samples.extend(rng.choice(indexes_with_label,
                                               size=n_samples,
                                               replace=False))

        self.selected_samples_ = np.array(selected_samples, dtype=np.uint64)
        return self

    def sample(self, X, y):
        check_is_fitted(self)

        if isinstance(X, np.ndarray) or sp.sparse.issparse(X):
            return X[self.selected_samples_], np.array(y)[self.selected_samples_]

        return np.array(X)[self.selected_samples_], np.array(y)[self.selected_samples_]

    def fit_resample(self, X, y):
        self.fit(y)

        return self.sample(X, y)
