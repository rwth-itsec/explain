import numpy as np
from sklearn.base import BaseEstimator, MetaEstimatorMixin, ClassifierMixin
from sklearn.ensemble import RandomForestClassifier


class CostSensitivity:
    def __init__(self, weighting_power):
        self.weighting_power = weighting_power

    def calculate(self, y):
        classes, counts = np.unique(y, return_counts=True)

        return {
            cls: np.math.pow(y.shape[0] / count, self.weighting_power)
            for cls, count in zip(classes, counts)
        }


class SimpleDelegatedEstimator(MetaEstimatorMixin, ClassifierMixin, BaseEstimator):
    def __init__(self, estimator):
        self.estimator = estimator

    # delegate every attribute call that isn't explicitly defined
    def __getattr__(self, attr_name):
        return getattr(self.estimator, attr_name)


class WeightedRandomForestClassifier(RandomForestClassifier):
    def fit(self,
            X,
            y,
            sample_weight=None):
        if self.class_weight is not None and hasattr(self.class_weight, 'calculate'):
            self.class_weight = self.class_weight.calculate(y)
        super().fit(X, y, sample_weight=sample_weight)
