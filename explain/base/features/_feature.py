from __future__ import annotations

from abc import ABC, abstractmethod
from enum import Enum
from functools import lru_cache
from typing import Callable
from typing import Iterable

# default would be 128
# as long as we prevent multiple threads from sharing one cache N should be enough for N features
from explain.settings import LRU_CACHE_SIZE


class FeatureReturnType(Enum):
    """Enumeration of different return types.

    """
    INTEGER = 1
    """Use for integer return types.
    
    """
    RATIONAL = 2
    """Use for rational return types.
    
    """
    SPECIAL = 3
    """Use for miscellaneous return types.
    
    """


class Feature(ABC):
    """Wrapped form of a feature allowing dynamic extraction from samples.

    Parameters
    ----------
    name : str
            Name that will be used to refer to this feature in text form.
    return_type : FeatureReturnType
            Value indicating whether this feature returns discrete, continuous or special values.

    """

    def __init__(self,
                 name: str,
                 return_type: FeatureReturnType):
        self._name = name
        self._return_type = return_type

    @property
    def return_type(self) -> FeatureReturnType:
        return self._return_type

    @property
    def name(self) -> str:
        return self._name

    @abstractmethod
    def evaluate(self,
                 sample: str):
        """Deterministic evaluation of the wrapped feature.

        Parameters
        ----------
        sample : str
            Sample.

        Returns
        -------
        feature_value : any
            Feature value.

        """


# TODO: Test impact of caching solution
class CachedCallableFeature(Feature):
    """Wrapped feature which uses a supplied function for its feature value calculation.
    Uses caching to prevent recalculations of dependencies between features.

    Parameters
    ----------
    name : str
        Name that will be used to refer to this feature in text form.
    return_type : FeatureReturnType
        Value indicating whether this feature returns discrete, continuous or special values.
    eval_func : Callable
        The return value of eval_func is used as feature value.

    """
    def __init__(self,
                 name: str,
                 return_type: FeatureReturnType,
                 eval_func: Callable):
        super().__init__(name=name,
                         return_type=return_type)
        self.eval_func = eval_func

    @lru_cache(maxsize=LRU_CACHE_SIZE)
    def evaluate(self, sample):
        """Evaluate the feature function.

        Parameters
        ----------
        sample : str
            Sample.

        Returns
        -------
        feature_value : any
            Feature value.

        """
        return self.eval_func(sample)


class FeatureCollection:
    """Collecting-class for similar features.

    Parameters
    ----------
    features : iterable of :py:class:`Feature`
        Similar features to be bundled together.

    """

    def __init__(self,
                 features: Iterable[Feature]):
        self._features = features

    @property
    def features(self) -> Iterable[Feature]:
        return self._features

    def unpack(self, subset: Iterable[str] = None):
        """Returns features in this collection specified by feature names.

        Parameters
        ----------
        subset : list of str
            Feature names.

        Returns
        -------
        feature_list : list of :py:class:`Feature`
            Features of this collection, specified by feature names in `subset`.

        """
        return [feature for feature in self._features
                if subset is None
                or feature.name in subset]

    @classmethod
    def generate(cls, func, **params):
        def _func(sample):
            return func(sample, **params)

        return _func
