"""
Utility Functions and Features
------------------------------
"""

import functools
from typing import Iterable

from explain.base.features import CachedCallableFeature, FeatureReturnType
from explain.settings import VALID_SUFFIXES

__all__ = [
    'features_from_str_list',
    'safe_subdomains_suffix_split',
    'SafeSubdomainsSuffixSplit'
]


def features_from_str_list(file, feature_pool):
    """
    Maps a collection of feature names to their respective :py:class:`.Feature` objects from a pool of features.

    Parameters
    ----------
    file: list of str
       list of feature names to map to their respective objects from `feature_pool`
    feature_pool: list of :py:class:`.Feature`
       list of feature objects getting mapped to via their names

    Returns
    -------
    features: list of :py:class:`.Feature`
       list of mapped feature objects
    """

    features = []

    str_mapping = {
        feature.name: feature
        for feature in feature_pool
    }

    for line in file:
        feature = line.strip().lower()

        if feature in str_mapping:
            features.append(str_mapping[feature])
        else:
            raise ValueError(f'Unknown feature {feature}.')

    return features


def safe_subdomains_suffix_split(domain: str, valid_suffixes: Iterable[str]):
    """Utility function to split a given domain into a subdomain list and its longest public suffix.

    Parameters
    ----------
    domain: str
            the sample the split is performed on
    valid_suffixes: iterable
            valid suffixes to decide on the split position

    Returns
    -------
    subdomains : list
        the sample's subdomains
    public_suffix : str
        the sample's public suffix

    See Also
    --------
    SafeSubdomainsSuffixSplit : Feature evaluating :py:func:`safe_subdomains_suffix_split` as value.

    Notes
    -----
    This function is adapted from [1]_'s source code.
    In Explain's code we exclude the root domain from any consideration.
    Additionally top-level domains are treated as suffix-less, while for samples with invalid
    public suffix we expect the rightmost subdomain to be its public suffix.

    References
    ----------
    .. [1] S. Schuppen, D. Teubert, P. Herrmann, and U. Meyer, "FANCI:
           Feature-based Automated NXDomain Classification Intelligence"
           in USENIX Security Symposium, 2018.

    Examples
    --------
    >>> safe_subdomains_suffix_split('ieee-security.org', ['org', ..])
    (['ieee-security'], 'org')

    Top-level domains are treated as suffix-less:

    >>> safe_subdomains_suffix_split('org', ['org', ..])
    (['org'], '')

    If no valid public suffix can be found, the rightmost subdomain is treated as such instead,
    given the number of subdomains is greater than one:

    >>> safe_subdomains_suffix_split('ieee-security.invalid', ['org', ..])
    (['ieee-security'], 'invalid')
    >>> safe_subdomains_suffix_split('invalid', ['org', ..])
    (['invalid'], '')

    """

    dot_split = domain.split('.')
    dns_level = len(dot_split)

    if dns_level < 2:
        return dot_split, ''

    longest_public_suffix = ''
    for level in range(1, dns_level):
        considered_suffix_split = dot_split[level:]
        longest_public_suffix = '.'.join(considered_suffix_split)

        if longest_public_suffix in valid_suffixes:
            return dot_split[:level], longest_public_suffix

    return dot_split[:-1], longest_public_suffix


SafeSubdomainsSuffixSplit = CachedCallableFeature(name='safe-subdomains-suffix-split',
                                                  return_type=FeatureReturnType.SPECIAL,
                                                  eval_func=functools.partial(safe_subdomains_suffix_split,
                                                                              valid_suffixes=VALID_SUFFIXES))
"""Splits the sample into a tuple of subdomains and the potential longest public suffix.

See Also
--------
safe_subdomains_suffix_split : Utility function used internally with the ability to specify a list of valid suffixes.

Examples
--------
>>> SafeSubdomainsSuffixSplit.evaluate('ieee-security.org')
(['ieee-security'], 'org')

"""
