"""
Example Utility Features
------------------------
"""

from functools import lru_cache

import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

from explain.base.features import CachedCallableFeature, FeatureReturnType, FeatureCollection
from explain.base.features.utility import SafeSubdomainsSuffixSplit

__all__ = [
    'CONSONANTS',
    'DECIMAL_DIGITS',
    'HEXADECIMAL_DIGITS',
    'HYPHENS',
    'PRIME_DIGITS',
    'VOWELS',

    'JoinedSubdomains',
    'SuffixSplit',
    'JoinedSuffix',
    'JoinedSubdomainsBitArray',
    'JoinedSubdomainsUnicode',
    'JoinedSubdomainsUnicodeBitarray',
    'JoinedSubdomainsAlphabet',
    'JoinedSubdomainsNGrams',

    'DL_VALID_CHARS',
    'HOSTNAME_UNIGRAM'
]

# token mapping used as in deep learning approaches
DL_VALID_CHARS = {'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8, 'i': 9, 'j': 10, 'k': 11, 'l': 12,
                  'm': 13, 'n': 14, 'o': 15, 'p': 16, 'q': 17, 'r': 18, 's': 19, 't': 20, 'u': 21, 'v': 22, 'w': 23,
                  'x': 24, 'y': 25, 'z': 26, '0': 27, '1': 28, '2': 29, '3': 30, '4': 31, '5': 32, '6': 33, '7': 34,
                  '8': 35, '9': 36, '-': 37, '_': 38, '.': 39}

CONSONANTS         = 'bcdfghjklmnpqrstvwxyz'
DECIMAL_DIGITS     = '0123456789'
HEXADECIMAL_DIGITS = '0123456789abcdef'
HYPHENS            = '-'
PRIME_DIGITS       = '2357bdhjntv'
VOWELS             = 'aeiou'

HOSTNAME_CHARACTERS = list(sorted(VOWELS + CONSONANTS + DECIMAL_DIGITS + HYPHENS))


def _join_subdomains(sample):
    return ''.join(SafeSubdomainsSuffixSplit.evaluate(sample)[0])


JoinedSubdomains = CachedCallableFeature(name='joined-subdomains',
                                         return_type=FeatureReturnType.SPECIAL,
                                         eval_func=_join_subdomains)
"""Removes the public suffix and all separating dots from the sample.

See Also
--------
JoinedSubdomainsBitArray :
    Evaluates the UTF-8 encoded binary sequence of :py:data:`JoinedSubdomains`.
JoinedSuffix :
    Evaluates the public suffix without separating dots.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> JoinedSubdomains.evaluate('ieee-security.org')
'ieee-security'

Multiple subdomains are concatenated:

>>> JoinedSubdomains.evaluate('multiple.subdomains.ieee-security.org')
'multiplesubdomainsieee-security'

"""


def _safe_suffix_split(sample):
    suffix = SafeSubdomainsSuffixSplit.evaluate(sample)[1]

    if not suffix:
        return []
    return suffix.split('.')


SuffixSplit = CachedCallableFeature(name='suffix-split',
                                    return_type=FeatureReturnType.SPECIAL,
                                    eval_func=_safe_suffix_split)
"""Splits the public suffix into its subdomains.

See Also
--------
JoinedSubdomains :
    Evaluates the concatenation of the subdomains without separating dots.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> SuffixSplit.evaluate('ieee-security.co.uk')
['co', 'uk']

For top-level domains this feature returns an empty list:

>>> SuffixSplit.evaluate('org')
[]

"""


def _join_suffix(sample):
    return ''.join(SuffixSplit.evaluate(sample))


JoinedSuffix = CachedCallableFeature(name='joined-suffix',
                                     return_type=FeatureReturnType.SPECIAL,
                                     eval_func=_join_suffix)
"""Computes the public suffix without separating dots.

See Also
________
JoinedSubdomains :
    Evaluates the concatenation of the subdomains without separating dots.
    
Notes
-----
This feature depends on :py:data:`SuffixSplit`.

Examples
--------
>>> JoinedSuffix.evaluate('ieee-security.co.uk')
'couk'

"""


def _subdomains_bitarray(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    return np.unpackbits(np.array(joined_subdomains, 'c').view(np.uint8))


JoinedSubdomainsBitArray = CachedCallableFeature(name='joined-subdomains-bitarray',
                                                 return_type=FeatureReturnType.SPECIAL,
                                                 eval_func=_subdomains_bitarray)
"""Transforms the sample into the UTF-8 encoded binary sequence of its joined subdomains.

See Also
________
JoinedSubdomainsUnicodeBitarray :
    Evaluates the UTF-8 encoded binary sequence of :py:data:`JoinedSubdomainsUnicode`.

Notes
-----
Each encoded character of the subdomains is padded to 8 bits.

This feature depends on :py:data:`JoinedSubdomains`.

Examples
--------
>>> JoinedSubdomainsBitArray.evaluate('ieee-security.org')
array([0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1,
       0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 1,
       0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1,
       1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1,
       0, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1], dtype=uint8)

"""


def _subdomains_unicode(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    return [DL_VALID_CHARS[character] + 96 for character in joined_subdomains if character not in '-_']


def _subdomains_unicode_bitarray(sample):
    subdomains_unicode = JoinedSubdomainsUnicode.evaluate(sample)
    return np.unpackbits(np.array(subdomains_unicode, dtype=np.uint8))


JoinedSubdomainsUnicode = CachedCallableFeature(name='joined-subdomains-unicode',
                                                return_type=FeatureReturnType.SPECIAL,
                                                eval_func=_subdomains_unicode)
"""Constructs a modified Unicode code point list of the concatenated subdomains.

Notes
-----
It ignores underscores and hyphens and alters the code points of decimal digits.
Instead of code points between 48 (`"0"`) and 58 (`"9"`) the points are shifted to continue the latin character sequence:
123 (`"0"`) to 132 (`"9"`).

This feature depends on :py:data:`JoinedSubdomains`.

Examples
--------
>>> JoinedSubdomainsUnicode.evaluate('ieee-security.org')
[105, 101, 101, 101, 115, 101, 99, 117, 114, 105, 116, 121]

"""

JoinedSubdomainsUnicodeBitarray = CachedCallableFeature(name='joined-subdomains-unicode-bitarray',
                                                        return_type=FeatureReturnType.SPECIAL,
                                                        eval_func=_subdomains_unicode_bitarray)
"""Calculates the binary representation of the altered code point list.

See Also
________
JoinedSubdomainsBitArray :
    Evaluates the UTF-8 encoded binary sequence of :py:data:`JoinedSubdomains`.

Notes
-----
Each encoded character of the subdomains is padded to 8 bits.

This feature depends on :py:data:`JoinedSubdomainsUnicode`.


Examples
--------
>>> JoinedSubdomainsUnicode.evaluate('ieee-security.org')
array([0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 0, 0, 1,
       0, 1, 0, 1, 1, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0,
       0, 1, 0, 1, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 1,
       1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0,
       0, 1, 1, 1, 1, 0, 0, 1], dtype=uint8)

"""

HOSTNAME_UNIGRAM = CountVectorizer(analyzer='char',
                                   ngram_range=(1, 1),
                                   vocabulary=HOSTNAME_CHARACTERS)


def _to_1_gram(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    return HOSTNAME_UNIGRAM.fit_transform([joined_subdomains]).toarray()


# TODO: reference HOSTNAME_CHARACTERS
JoinedSubdomainsAlphabet = CachedCallableFeature(name='joined-subdomains-alphabet',
                                                 return_type=FeatureReturnType.SPECIAL,
                                                 eval_func=_to_1_gram)
"""Calculates the unigram of the concatenated subdomains using the set of valid hostname characters as alphabet.

Notes
-----
This features depends on :py:data:`JoinedSubdomains`.

Examples
--------
>>> JoinedSubdomainsAlphabet.evaluate('ieee-security.org')
array([[1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 4, 0, 0, 0, 2, 0, 0,
        0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 0]], dtype=int64)
"""


@lru_cache(maxsize=3)
def _cached_vectorizer(n):
    return CountVectorizer(analyzer='char', ngram_range=(n, n))


def _n_gram(sample, n):
    vectorizer = _cached_vectorizer(n)

    joined_subdomains = JoinedSubdomains.evaluate(sample)
    try:
        return vectorizer.fit_transform([joined_subdomains]).toarray()
    except ValueError:
        return np.array([])


_ngrams = []
for n in range(1, 4):
    _ngram = CachedCallableFeature(name=f'{n}-gram',
                                   return_type=FeatureReturnType.SPECIAL,
                                   eval_func=FeatureCollection.generate(_n_gram, **{'n': n}))
    _ngram.n = n
    _ngrams.append(_ngram)

JoinedSubdomainsNGrams = FeatureCollection(_ngrams)
"""Calculates ngrams of the joined subdomains.

This is a collection of 3 utility features calculating unigrams, bigrams and trigrams.

See Also
--------
JoinedSubdomainsAlphabet :
    Evaluates the unigram of the subdomains concatenation with fixed alphabet.

Notes
-----
The identifiers of the features are {1, 2, 3}-gram.

The features depend on :py:data:`JoinedSubdomains`.

Examples
--------
>>> JoinedSubdomainsNGrams.unpack(['2-gram'])[0].evaluate('ieee-security.org')
array([[1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1]], dtype=int64)

"""
