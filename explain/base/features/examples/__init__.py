from explain.base.features.examples.linguistic import *
from explain.base.features.examples.statistical import *
from explain.base.features.examples.structural import *

ALL = [
    BitsEntropy,
    *NgramStatisticalFunctions.unpack(),
    *RandomnessTests.unpack(),
    ZlibBitsCompressionRatio,

    ContainsIPv4,
    DigitExclusiveSubdomainsRatio,
    DomainNameLength,
    HexExclusiveSubdomainsRatio,
    PrefixRepetitionExclusive,
    SecondLevelLength,
    SecondLevelRepeatedPrefix,
    SingleCharacterSubdomain,
    # SubdomainsContainSuffix,
    SubdomainsDNSLevel,
    SubdomainsLength,
    SubdomainsMeanLength,
    SuffixDNSLevel,
    SuffixLength,
    TLDInSubdomains,
    ValidTLD,
    WWWPresent,

    AdjacentDuplicatesRatio,
    *AlphabetFeatures.unpack(),
    *CharacterRatioFeatures.unpack(),
    *ConsecutiveCharactersRatios.unpack(),
    ConsonantToVowelRatio,
    ContainsDigits,
    *DigitSums.unpack(),
    FirstCharacterPair,
    *FirstDigitDistances.unpack(),
    InverseHammingDistance,
    *MaxStreakLengthFeatures.unpack(),
    RepeatedCharactersRatio,
    SuffixStandardDeviation,
    SyllableCount,
    WeightedStreaks
]
