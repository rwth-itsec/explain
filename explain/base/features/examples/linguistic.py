"""
Example Linguistic Features
---------------------------
"""
import string

import numpy as np
import scipy as sp
import scipy.spatial

from explain.base.features import FeatureCollection, CachedCallableFeature, FeatureReturnType
from explain.base.features.examples.structural import SubdomainsLength
from explain.base.features.examples.utility import CONSONANTS, DECIMAL_DIGITS, HEXADECIMAL_DIGITS, HOSTNAME_UNIGRAM, \
    HYPHENS, PRIME_DIGITS, VOWELS, JoinedSubdomains, JoinedSubdomainsAlphabet, JoinedSubdomainsNGrams, JoinedSuffix
from explain.external.hyphenate import hyphenate_word

__all__ = [
    'AdjacentDuplicatesRatio',
    'AlphabetFeatures',
    'CharacterRatioFeatures',
    'ConsecutiveCharactersRatios',
    'ConsonantToVowelRatio',
    'ContainsDigits',
    'DigitSums',
    'FirstCharacterPair',
    'FirstDigitDistances',
    'InverseHammingDistance',
    'MaxStreakLengthFeatures',
    'RepeatedCharactersRatio',
    'SuffixStandardDeviation',
    'SyllableCount',
    'WeightedStreaks'
]


def _alphabet_extract(sample, index):
    return JoinedSubdomainsAlphabet.evaluate(sample)[0, index]


def _format_alphabet_feature_name(character):
    if character == '-':
        return 'alphabet-hyphen'
    return f'alphabet-{character}'


AlphabetFeatures = FeatureCollection(
    [CachedCallableFeature(name=_format_alphabet_feature_name(character),
                           return_type=FeatureReturnType.INTEGER,
                           eval_func=FeatureCollection.generate(_alphabet_extract, **{'index': index}))
     for index, character in enumerate(HOSTNAME_UNIGRAM.vocabulary)]
)
"""Evaluates the number of occurrences of hostname characters in the subdomains.

This is a collection of 37 features that return integer values.

Notes
-----
Each of the feature counts the number occurrences of a single character of the valid hostname alphabet, excluding dots.
The identifiers of the features are according to the counted character:
alphabet-{a, b, c, ..., z, 0, ..., 9, hyphen}

The features depend on :py:data:`.JoinedSubdomainsAlphabet`.


Examples
--------
>>> import string
>>> vocal_count = AlphabetFeatures.unpack([f'alphabet-{i}' for i in 'aeiou'])
>>> sample = 'ieee-security.org'
>>> for alphabet_feature in vocal_count:
>>>     print(alphabet_feature.evaluate(sample))
0
4
2
0
1

The feature responsible for hyphens uses 'alphabet-hyphen' as identifier:

>>> AlphabetFeatures.unpack(['alphabet-hyphen'])[0].evaluate(sample)
1

"""

ALPHABETS = {
    'consonants': CONSONANTS,
    'decimaldigits': DECIMAL_DIGITS,
    'hexadecimaldigits': HEXADECIMAL_DIGITS,
    'hyphens': HYPHENS,
    'primedigits': PRIME_DIGITS,
    'vowels': VOWELS,
    'underscore': '_'
}


def _character_ratio(sample, alphabet):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    joined_subdomains_length = SubdomainsLength.evaluate(sample)

    characters_count = 0
    for character in joined_subdomains:
        if character in alphabet:
            characters_count += 1

    return characters_count / joined_subdomains_length


# TODO: alphabets
CharacterRatioFeatures = FeatureCollection(
    [CachedCallableFeature(name=f'{alphabet_name}-character-ratio',
                           return_type=FeatureReturnType.RATIONAL,
                           eval_func=FeatureCollection.generate(_character_ratio, **{'alphabet': alphabet}))
     for alphabet_name, alphabet in ALPHABETS.items()]
)
"""Evaluates the ratio of characters from different alphabets in the subdomains.

This is a collection of 7 features with the same underlying function, but different parameters.
They return rational values in [0.0, 1.0].

See Also
--------
MaxStreakLengthFeatures :
    Evaluates the longest consecutive streak of characters in the subdomains.

Notes
-----
Each of the feature calculates the ratio of characters from different alphabets in the subdomains.
The identifiers of the features are according to the used alphabet:
{consonants, decimaldigits, hexadecimaldigits, hyphens, primedigits, vowels, underscore}-character-ratio.

The features depend on :py:data:`.JoinedSubdomains` and :py:data:`.SubdomainsLength`.

Examples
--------
>>> sample = 'ieee-security.org'
>>> for character_ratio_feature in CharacterRatioFeatures.unpack():
>>>     print(character_ratio_feature.name, character_ratio_feature.evaluate(sample))
consonants-character-ratio 0.38461538461538464
decimaldigits-character-ratio 0.0
hexadecimaldigits-character-ratio 0.38461538461538464
hyphens-character-ratio 0.07692307692307693
primedigits-character-ratio 0.07692307692307693
vowels-character-ratio 0.5384615384615384
underscore-character-ratio 0.0

"""


def _longest_streak(val, alphabet):
    current_streak = 0
    longest_streak = 0

    for character in val:
        if character in alphabet:
            current_streak += 1
        else:
            longest_streak = max(longest_streak, current_streak)
            current_streak = 0
    return max(longest_streak, current_streak)


def _max_streak_length(sample, alphabet):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    return _longest_streak(joined_subdomains, alphabet)


MaxStreakLengthFeatures = FeatureCollection(
    [CachedCallableFeature(name=f'{alphabet_name}-max-streak-length',
                           return_type=FeatureReturnType.INTEGER,
                           eval_func=FeatureCollection.generate(_max_streak_length, **{'alphabet': alphabet}))
     for alphabet_name, alphabet in ALPHABETS.items()
     if alphabet_name != 'underscore']
)
"""Evaluates the longest consecutive sequence of characters from different alphabets in the subdomains.

This is a collection of 7 features that return integer values.

See Also
--------
CharacterRatioFeatures :
    Evaluates the ratio of characters in the subdomains.

Notes
-----
Each of the features calculates the the longest consecutive sequence of characters from different alphabets in the subdomains.
The identifiers of the features are according to the used alphabet:
{consonants, decimaldigits, hexadecimaldigits, hyphens, primedigits, vowels}-max-streak-length.

The features depend on :py:data:`.JoinedSubdomains`.

Examples
--------
>>> sample = 'ieee-security.org'
>>> for max_streak_length_feature in MaxStreakLengthFeatures.unpack():
>>>     print(max_streak_length_feature.name, max_streak_length_feature.evaluate(sample))
consonants-max-streak-length 2
decimaldigits-max-streak-length 0
hexadecimaldigits-max-streak-length 3
hyphens-max-streak-length 1
primedigits-max-streak-length 1
vowels-max-streak-length 4

"""


def _adjacent_duplicates_ratio(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    joined_subdomains_length = SubdomainsLength.evaluate(sample)

    duplicates = 0
    for c1, c2 in zip(joined_subdomains[:-1], joined_subdomains[1:]):
        if c1 == c2:
            duplicates += 1

    return duplicates / joined_subdomains_length


AdjacentDuplicatesRatio = CachedCallableFeature(name='adjacent-duplicates-ratio',
                                                return_type=FeatureReturnType.RATIONAL,
                                                eval_func=_adjacent_duplicates_ratio)
"""Evaluates the ratio of duplicate characters in all substrings of the concatenated subdomains with length two.

This feature returns rational values in [0, 1).

Notes
-----
This feature depends on :py:data:`.JoinedSubdomains` and :py:data:`.SubdomainsLength`.
 
Examples
--------
>>> AdjacentDuplicatesRatio.evaluate('ieee-security.org')
0.15384615384615385
 
The length two substrings in this example are ['ie', 'ee', 'ee', 'e-', '-s', 'se', 'ec', 'cu', 'ur', 'ri', 'it', 'ty'].
 
"""


def _first_digit(sample, start_position):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    direction = -1 if start_position == 'End' else 1

    for distance, character in enumerate(joined_subdomains[::direction]):
        if character in DECIMAL_DIGITS:
            return distance

    return -1


FirstDigitDistances = FeatureCollection(
    [CachedCallableFeature(name=f'{start_position}-digit-edge-distance',
                           return_type=FeatureReturnType.INTEGER,
                           eval_func=FeatureCollection.generate(_first_digit, **{'start_position': start_position}))
     for start_position in ['end', 'start']]
)
"""Evaluates the position of the first digit in the concatenated subdomains from either of the two string ends.

This is a collection of 2 features that return integer values.
For domains without any digits the features return -1.

Notes
-----
The features are referred to by the names {end, start}-digit-edge-distance.
While `start-digit-edge-distance` evaluates the position starting at the leftmost character, `end-digit-edge-distance` is starting at the rightmost character.

The features depend on :py:data:`.JoinedSubdomains`.

Examples
--------
>>> sample = '123.ieee-security.org'
>>> for distance in FirstDigitDistances.unpack():
>>>     print(distance.name, distance.evaluate(sample))
end-digit-edge-distance 13
start-digit-edge-distance 0

"""


def _first_character_pair_encoding(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    joined_subdomains_length = SubdomainsLength.evaluate(sample)

    a = ord(joined_subdomains[0])
    b = ord(joined_subdomains[1]) if joined_subdomains_length > 1 else -1

    return 2 * a + b * b


FirstCharacterPair = CachedCallableFeature(name='first-character-pair',
                                           return_type=FeatureReturnType.INTEGER,
                                           eval_func=_first_character_pair_encoding)
"""Evaluates a numeric encoding of the first two characters of the concatenated subdomains.

The encoding adheres to the function 2 * ord(a) + ord(b)^2 where a is the first character, b the second and ord() the function that transforms a character into its Unicode code point.
This feature returns positive integer values between 91 and 15128.

Notes
-----
For the empty character ord() is adjusted to return -1.
The used function is injective for any possible two character pairs (including the empty one).

This feature depends on :py:data:`.JoinedSubdomains` and :py:data:`.SubdomainsLength`.

Examples
--------
>>> FirstCharacterPair.evaluate('ieee-security.org')
10411

"""


def _hamming_distance(sample):
    joined_subdomains = list(JoinedSubdomains.evaluate(sample))
    return sp.spatial.distance.hamming(joined_subdomains,
                                       joined_subdomains[::-1])


InverseHammingDistance = CachedCallableFeature(name='inverse-hamming-distance',
                                               return_type=FeatureReturnType.RATIONAL,
                                               eval_func=_hamming_distance)
"""Evaluates the proportion of disagreeing characters of the concatenated subdomains and its inverse.

This feature returns rational numbers in [0.0, 1.0].

Notes
-----
This feature depends on :py:data:`.JoinedSubdomains`.

Examples
--------
>>> InverseHammingDistance.evaluate('ieee-security.org')
0.9230769230769231

"""


def _suffix_standard_deviation(sample):
    joined_suffix = JoinedSuffix.evaluate(sample)
    if len(joined_suffix) == 0:
        return -1

    return np.std([ord(c) for c in joined_suffix])


SuffixStandardDeviation = CachedCallableFeature(name='suffix-standard-deviation',
                                                return_type=FeatureReturnType.RATIONAL,
                                                eval_func=_suffix_standard_deviation)
"""Evaluates the standard deviation of the code points of the joined suffix.

This feature returns rational numbers.

Notes
-----
This feature depends on :py:data:`.JoinedSuffix`.

Examples
--------
>>> SuffixStandardDeviation.evaluate('ieee-security.org')
4.642796092394706

"""

WEIGHTED_STREAKS_ALPHABET = CONSONANTS + DECIMAL_DIGITS
WEIGHTED_STREAKS_MULTIPLIER = 2.0
WEIGHTED_STREAKS_UPPER_BOUND = np.finfo(np.float32).max


def _weighted_streaks(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    joined_subdomains_length = SubdomainsLength.evaluate(sample)

    weighted_sum = 0
    streak_bonus = 1

    for i, character in enumerate(joined_subdomains):
        distance_weight = 1 - i / joined_subdomains_length
        weighted_sum += distance_weight * streak_bonus

        if character in WEIGHTED_STREAKS_ALPHABET:
            streak_bonus *= WEIGHTED_STREAKS_MULTIPLIER
        else:
            streak_bonus = 1

    weighted_sum = np.minimum(WEIGHTED_STREAKS_UPPER_BOUND, weighted_sum)

    return weighted_sum / joined_subdomains_length


WeightedStreaks = CachedCallableFeature(name='weighted-streaks',
                                        return_type=FeatureReturnType.RATIONAL,
                                        eval_func=_weighted_streaks)
"""Evaluates a custom function that calculates a score that includes various different metrics.

This feature calculates a rational sum of exponential growing weights.
Those weights grow for each consecutive consonant or decimal number and reset for any other character.
In addition, the relative position of each character is included in the calculation of the weights.

Notes
-----
To prevent edge cases from throwing errors, this feature is bounded by the maximum value for a 32-bit floating-point number.

This feature depends on :py:data:`.JoinedSubdomains` and :py:data:`.SubdomainsLength`.

Examples
--------
>>> WeightedStreaks.evaluate('ieee-security.org')
0.6331360946745561

"""

ConsonantsCharacterRatio = CharacterRatioFeatures.unpack(['consonants-character-ratio'])[0]
VowelsCharacterRatio = CharacterRatioFeatures.unpack(['vowels-character-ratio'])[0]


def _consonant_to_vowel_ratio(sample):
    consonants_ratio = ConsonantsCharacterRatio.evaluate(sample)
    vowels_ratio = VowelsCharacterRatio.evaluate(sample)

    if vowels_ratio == 0:
        joined_subdomains_length = SubdomainsLength.evaluate(sample)

        # Instead of returning infinity / NaN return a valid number instead
        return consonants_ratio * joined_subdomains_length

    return consonants_ratio / vowels_ratio


ConsonantToVowelRatio = CachedCallableFeature(name='consonant-to-vowel-ratio',
                                              return_type=FeatureReturnType.RATIONAL,
                                              eval_func=_consonant_to_vowel_ratio)
"""Evaluates the ratio of consonants to vowels in the concatenated subdomains.

This feature returns rational values.


See Also
--------
CharacterRatioFeatures :
    Evaluates the ratio of characters in the subdomains.

Notes
-----
Instead of returning an invalid number (NaN/inf), this feature returns the number of consonants instead for samples without any vowels.

This feature depends on :py:data:`CharacterRatioFeatures`'s `consonants-character-ratio` and `vowels-character-ratio`.

Examples
--------
>>> ConsonantToVowelRatio.evaluate('ieee-security.org')
0.7142857142857144

"""

DIGIT_SUM_EXCLUSION = '-_'


def _digit_sum(sample, sample_part):
    digit_sum = 0

    if sample_part == 'subdomain':
        character_sequence = JoinedSubdomains.evaluate(sample)
    else:
        character_sequence = JoinedSuffix.evaluate(sample)

    for character in character_sequence:
        if character not in DIGIT_SUM_EXCLUSION:
            digit_sum += int(character, 36)

    return digit_sum


DigitSums = FeatureCollection([
    CachedCallableFeature(name=f'{sample_part}-digit-sum',
                          return_type=FeatureReturnType.INTEGER,
                          eval_func=FeatureCollection.generate(_digit_sum, **{'sample_part': sample_part}))
    for sample_part in ['subdomains', 'suffix']
])
"""Evaluates the sum of the characters interpreted as base 36 digits - excluding dots, hyphens and underscores.

This is a collection of 2 features with the same underlying function, but different parameters.
They return integer values.

Notes
-----
The features are referred to by the names {subdomains, suffix}-digit-sum.
While `subdomains-digit-sum` evaluates the sum using the subdomains, `suffix-digit-sum` calculates them using the public suffix.

The features depend on :py:data:`.JoinedSubdomains` and :py:data:`.JoinedSuffix`.

Examples
--------
>>> sample = 'ieee-security.org'
>>> for digit_sum in DigitSums.unpack():
>>>    print(digit_sum.name, digit_sum.evaluate(sample))
subdomains-digit-sum 252
suffix-digit-sum 67

"""


def _syllable_count(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    only_ascii_letters = ''.join([c for c in joined_subdomains if c in string.ascii_lowercase])

    # estimation using tex's \hyphenation{} algorithm (by Frank Liang, implemented by Ned Batchelder)
    tex_hyphenated = hyphenate_word(only_ascii_letters)
    return len(tex_hyphenated)


SyllableCount = CachedCallableFeature(name='syllable-count',
                                      return_type=FeatureReturnType.INTEGER,
                                      eval_func=_syllable_count)
"""Evaluates the approximate number of syllables in the concatenated subdomains.

This feature returns integer values.

Notes
-----
In its evaluation the feature excludes any character not belonging to the latin alphabet.
The returned values are approximations using the hyphenation algorithm by Frank Liang in a version implemented by Ned Batchelder.

This feature depends on :py:data:`.JoinedSubdomains`.

Examples
--------
>>> SyllableCount.evaluate('ieee-security.org')
4

"""


def _contains_digits(sample):
    return any(character.isdigit() for character in sample)


ContainsDigits = CachedCallableFeature(name='contains-digits',
                                       return_type=FeatureReturnType.INTEGER,
                                       eval_func=_contains_digits)
"""Evaluates the presence of a decimal digit in the sample.

This feature returns binary values in {0, 1}. 
If any character of the sample is a decimal digit, this feature returns 1, otherwise 0.

Examples
--------
>>> ContainsDigits.evaluate('ieee-security.org')
False

"""

UNIGRAM = JoinedSubdomainsNGrams.unpack(['1-gram'])[0]


def _repeated_characters_ratio(sample):
    unigram = UNIGRAM.evaluate(sample)
    return np.sum(unigram > 1) / unigram.size


RepeatedCharactersRatio = CachedCallableFeature(name='repeated-characters-ratio',
                                                return_type=FeatureReturnType.RATIONAL,
                                                eval_func=_repeated_characters_ratio)
"""Evaluates the ratio of characters in the concatenated subdomains that occur at least twice.

This feature returns rational values in [0.0, 1.0].

Notes
-----
This feature depends on :py:data:`.JoinedSubdomainsNGrams`'s `1-gram`.

Examples
--------
>>> RepeatedCharactersRatio.evaluate('ieee-security.org')
0.2222222222222222

"""


def _consecutive_characters_ratio(sample, alphabet):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    joined_subdomains_length = SubdomainsLength.evaluate(sample)

    total_consecutive = 0
    tmp_consecutive = 0

    for character in joined_subdomains:
        if character in alphabet:
            tmp_consecutive += 1
        else:
            if tmp_consecutive > 1:
                total_consecutive += tmp_consecutive
            tmp_consecutive = 0

    if tmp_consecutive > 1:
        total_consecutive += tmp_consecutive

    return total_consecutive / joined_subdomains_length


ConsecutiveCharactersRatios = FeatureCollection([
    CachedCallableFeature(name=f'consecutive-{alphabet_name}-ratio',
                          return_type=FeatureReturnType.RATIONAL,
                          eval_func=FeatureCollection.generate(_consecutive_characters_ratio, **{'alphabet': alphabet}))
    for alphabet_name, alphabet in {
        'consonant': CONSONANTS,
        'digit': DECIMAL_DIGITS
    }.items()
])
"""Evaluates the ratio of the sum of all streaks of either consecutive consonants or decimal digits.

This is a collection of 2 features with the same underlying function, but different parameters.
They return rational values.

Notes
-----
The features are referenced by the names consecutive-{consonant, digit}-ratio.

The features depend on :py:data:`.JoinedSubdomains` and :py:data:`.SubdomainsLength`.


Examples
--------
>>> sample = 'ieee-security.org'
>>> for consecutive_character_ratio in ConsecutiveCharactersRatio.unpack():
>>>     print(consecutive_character_ratio.name, consecutive_character_ratio.evaluate(sample))
consecutive-consonant-ratio 0.15384615384615385
consecutive-digit-ratio 0.0

"""
