"""
Example Structural Features
---------------------------
"""

import functools
import operator
import re
from typing import Iterable

from explain.settings import VALID_SUFFIXES, VALID_TLDS
from explain.base.features import CachedCallableFeature, FeatureReturnType
from explain.base.features.examples.utility import DECIMAL_DIGITS, HEXADECIMAL_DIGITS, JoinedSubdomains, SuffixSplit
from explain.base.features.utility import SafeSubdomainsSuffixSplit

__all__ = [
    'ContainsIPv4',
    'DigitExclusiveSubdomainsRatio',
    'DomainNameLength',
    'HexExclusiveSubdomainsRatio',
    'PrefixRepetitionExclusive',
    'SingleCharacterSubdomain',
    'SecondLevelLength',
    'SecondLevelRepeatedPrefix',
    'SubdomainsContainSuffix',
    'SubdomainsDNSLevel',
    'SubdomainsLength',
    'SubdomainsMeanLength',
    'SuffixDNSLevel',
    'SuffixLength',
    'TLDInSubdomains',
    'ValidTLD',
    'WWWPresent'
]

# pattern of an IPv4 address adopted from FANCI's source code
IPv4_pattern = re.compile(r"(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}"
                          r"([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])")


def _contains_ipv4(sample):
    return IPv4_pattern.search(sample) is not None


ContainsIPv4 = CachedCallableFeature(name='contains-ipv4',
                                     return_type=FeatureReturnType.INTEGER,
                                     eval_func=_contains_ipv4)
"""Evaluates whether the sample contains a coherent IPv4 address.

This feature returns binary values in {0, 1}.
If the sample contains a coherent IPv4 address this feature returns 1, otherwise 0.

Examples
--------
>>> ContainsIPv4.evaluate('ieee-security.org')
False

The IPv4 address has to be coherent:

>>> ContainsIPv4.evaluate('192.168.188.38.ieee-security.org')
True
>>> ContainsIPv4.evaluate('192.168.188.ieee-security.38.org')
False

"""


def _exclusive_subdomains(sample: str, alphabet: Iterable):
    subdomains = SafeSubdomainsSuffixSplit.evaluate(sample)[0]
    alphabet_exclusive = 0

    for subdomain in subdomains:
        if all(map(functools.partial(operator.contains, alphabet), subdomain)):
            alphabet_exclusive += 1

    return alphabet_exclusive


def _exclusive_subdomains_ratio(sample: str, alphabet: Iterable):
    return _exclusive_subdomains(sample, alphabet) / SubdomainsDNSLevel.evaluate(sample)


DigitExclusiveSubdomainsRatio = CachedCallableFeature(name='digit-exclusive-subdomains-ratio',
                                                      return_type=FeatureReturnType.RATIONAL,
                                                      eval_func=functools.partial(_exclusive_subdomains_ratio,
                                                                                  alphabet=DECIMAL_DIGITS))
"""Evaluates the ratio of subdomains containing only decimal digits - excluding the public suffix.

This feature returns rational values in [0.0, 1.0].

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> DigitExclusiveSubdomainsRatio.evaluate('1.ieee-security.org')
0.5

"""

HexExclusiveSubdomainsRatio = CachedCallableFeature(name='hex-exclusive-subdomains-ratio',
                                                    return_type=FeatureReturnType.RATIONAL,
                                                    eval_func=functools.partial(_exclusive_subdomains_ratio,
                                                                                alphabet=HEXADECIMAL_DIGITS))
"""Evaluates the ratio of subdomains containing only hexadecimal digits - excluding the public suffix.

This feature returns rational values in [0.0, 1.0].

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> HexExclusiveSubdomainsRatio.evaluate('a.b.c.ieee-security.org')
0.75

"""


def _domain_name_length(sample):
    return len(sample)


DomainNameLength = CachedCallableFeature(name='domain-name-length',
                                         return_type=FeatureReturnType.INTEGER,
                                         eval_func=_domain_name_length)
"""Evaluates the total length of the sample.

This feature returns integer values.

See Also
--------
SecondLevelLength :
    Evaluates the total length of the first subdomain below the public suffix.
SubdomainsLength :
    Evaluates the combined length of all subdomains returned by :py:data:`.SafeSubdomainsSuffixSplit`.

Notes
-----
This feature includes separating dots in its calculation.

Examples
--------
>>> DomainNameLength.evaluate('ieee-security.org')
17

"""


def _contains_repeated_sequence(s: str):
    return (s + s).find(s, 1, -1) != -1


PrefixRepetitionExclusive = CachedCallableFeature(name='prefix-repetition-exclusive',
                                                  return_type=FeatureReturnType.INTEGER,
                                                  eval_func=_contains_repeated_sequence)
"""Evaluates whether the sample is a repeating sequence.

This feature returns binary values in {0, 1}. If the sample is a exclusively repeating sequence it returns 1, otherwise 0.

Examples
--------
>>> PrefixRepetitionExclusive.evaluate('ieee-security.orgieee-security.org')
True

"""


def _second_level_length(sample):
    subdomains = SafeSubdomainsSuffixSplit.evaluate(sample)[0]
    return len(subdomains[-1])


SecondLevelLength = CachedCallableFeature(name='second-level-length',
                                          return_type=FeatureReturnType.INTEGER,
                                          eval_func=_second_level_length)
"""Evaluates the length of the first subdomain below the public suffix.

This feature returns integer values.

See Also
--------
DomainNameLength :
    Evaluates the total length of the sample.
SubdomainsLength :
    Evaluates the combined length of all subdomains returned by :py:data:`.SafeSubdomainsSuffixSplit`.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> SecondLevelLength.evaluate('ieee-security.org')
13

"""


def _second_level_repeated_prefix(sample):
    subdomains = SafeSubdomainsSuffixSplit.evaluate(sample)[0]
    return _contains_repeated_sequence(subdomains[-1])


SecondLevelRepeatedPrefix = CachedCallableFeature(name='second-level-repeated-prefix',
                                                  return_type=FeatureReturnType.INTEGER,
                                                  eval_func=_second_level_repeated_prefix)
"""Evaluates whether the first subdomain below the public suffix is a repeating sequence.

This feature returns binary values in {0, 1}. If the subdomain is a repeating sequence it returns 1, otherwise 0.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> SecondLevelRepeatedPrefix.evaluate('ieee-securityieee-security.org')
True

"""


def _single_character_subdomain(sample):
    subdomains = SafeSubdomainsSuffixSplit.evaluate(sample)[0]
    return 1 in map(len, subdomains)


SingleCharacterSubdomain = CachedCallableFeature(name='single-character-subdomain',
                                                 return_type=FeatureReturnType.INTEGER,
                                                 eval_func=_single_character_subdomain)
"""Evaluates whether the sample has a subdomain of length 1 - excluding the public suffix.

This feature returns binary values in {0, 1}. If the sample has a subdomain of length 1 it returns 1, otherwise 0.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> SingleCharacterSubdomain.evaluate('ieee-security.org)
False

"""


DOT_FREE_SUFFIXES = set(suffix.replace('.', '') for suffix in VALID_SUFFIXES)


def _subdomains_contains_suffix(sample):
    return any([suffix in SafeSubdomainsSuffixSplit.evaluate(sample)[0] for suffix in DOT_FREE_SUFFIXES])


# TODO: slightly changed the definition / implementation as it wouldn't be distinguishable from TLDinSubdomains - determine if correct change
# Not considered in paper
SubdomainsContainSuffix = CachedCallableFeature(name="subdomains-contain-suffix",
                                                return_type=FeatureReturnType.INTEGER,
                                                eval_func=_subdomains_contains_suffix)
"""Evaluates whether one of the subdomains consists of the same character sequence as a public suffix.

This feature returns binary values in {0, 1}. 
If one of the subdomains has the same character sequence as a public suffix it returns 1, otherwise 0.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> SubdomainsContainSuffix.evaluate('couk.ieee-security.org')
True

"""


def _subdomains_dns_level(sample):
    subdomains = SafeSubdomainsSuffixSplit.evaluate(sample)[0]
    return len(subdomains)


SubdomainsDNSLevel = CachedCallableFeature(name='subdomains-dns-level',
                                           return_type=FeatureReturnType.INTEGER,
                                           eval_func=_subdomains_dns_level)
"""Evaluates the number of subdomains of the sample - excluding the public suffix.

This feature returns integer values.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> SubdomainsDNSLevel.evaluate('ieee-security.org')
1

"""


def _subdomains_length(sample):
    joined_subdomains = JoinedSubdomains.evaluate(sample)
    return len(joined_subdomains)


SubdomainsLength = CachedCallableFeature(name='subdomains-length',
                                         return_type=FeatureReturnType.INTEGER,
                                         eval_func=_subdomains_length)
"""Evaluates the combined length of all subdomains - excluding the public suffix.

This feature returns integer values.

See Also
--------
DomainNameLength :
    Evaluates the total length of the sample.
SecondLevelLength :
    Evaluates the length of the second level subdomain.
SuffixLength :
    Evaluates the combined length of the public suffix subdomains.

Notes
-----
This feature depends on :py:data:`.JoinedSubdomains`.

Examples
--------
>>> SubdomainsLength.evaluate('www.ieee-security.org')
16

"""


def _subdomains_mean_length(sample):
    subdomains_length = SubdomainsLength.evaluate(sample)
    subdomains_dns_level = SubdomainsDNSLevel.evaluate(sample)

    return subdomains_length / subdomains_dns_level


SubdomainsMeanLength = CachedCallableFeature(name='subdomains-mean-length',
                                             return_type=FeatureReturnType.RATIONAL,
                                             eval_func=_subdomains_mean_length)
"""Evaluates the mean length of the subdomains - excluding the public suffix.

This feature returns rational values.

Notes
-----
This feature depends on :py:data:`.SubdomainsLength` and :py:data:`.SubdomainsDNSLevel`.

Examples
--------
>>> SubdomainsMeanLength.evaluate('www.ieee-security.org')
8.0

"""


def _suffix_dns_level(sample):
    suffix_split = SuffixSplit.evaluate(sample)

    return len(suffix_split)


SuffixDNSLevel = CachedCallableFeature(name='suffix-dns-level',
                                       return_type=FeatureReturnType.INTEGER,
                                       eval_func=_suffix_dns_level)
"""Evaluates the number of subdomains belonging to the public suffix.

This feature returns integer values.

See Also
--------
SubdomainsDNSLevel :
    Evaluates the number of non public suffix subdomains.

Notes
-----
This feature depends on :py:data:`.SuffixSplit`.

Examples
--------
>>> SuffixDNSLevel.evaluate('ieee-security.org')
1

"""


def _suffix_length(sample):
    suffix_split = SuffixSplit.evaluate(sample)
    return sum(map(len, suffix_split))


SuffixLength = CachedCallableFeature(name='suffix-length',
                                     return_type=FeatureReturnType.INTEGER,
                                     eval_func=_suffix_length)
"""Evaluates the combined length of the subdomains belonging to the public suffix.

This feature returns integer values.

See Also
--------
SubdomainsLength :
    Evaluates the combined length of the non public suffix subdomains.

Notes
-----
Separating dots are excluded in the calculation.

This feature depends on :py:data:`.SuffixSplit`.

Examples
--------
>>> SuffixLength.evaluate('ieee-security.org')
3

"""


def _tld_in_subdomains(sample):
    subdomains = SafeSubdomainsSuffixSplit.evaluate(sample)[0]

    return any(subdomain in VALID_TLDS for subdomain in subdomains)


# TODO: decide if we keep it, practically == SubdomainsContainSuffix
TLDInSubdomains = CachedCallableFeature(name='tld-in-subdomains',
                                        return_type=FeatureReturnType.INTEGER,
                                        eval_func=_tld_in_subdomains)
"""Evaluates whether one of the subdomains consists of the same character sequence as a top-level domain.

This feature returns binary values in {0, 1}. 
If one of the subdomains has the same character sequence as a top-level domain it returns 1, otherwise 0.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> TLDInSubdomains.evaluate('org.ieee-security.org')
True

"""


def _valid_tld(sample):
    suffix_split = SuffixSplit.evaluate(sample)

    if suffix_split:
        return suffix_split[-1] in VALID_TLDS

    return False


ValidTLD = CachedCallableFeature(name='valid-tld',
                                 return_type=FeatureReturnType.INTEGER,
                                 eval_func=_valid_tld)
"""Evaluates whether the sample has a valid top-level domain.

This feature returns binary values in {0, 1}.
If the sample has a valid top-level domain it returns 1, otherwise 0.

Notes
-----
This feature depends on :py:data:`.SuffixSplit`.

Examples
--------
>>> ValidTLD.evaluate('ieee-security.invalid')
False

"""


def _www_present(sample):
    subdomains = SafeSubdomainsSuffixSplit.evaluate(sample)[0]

    return 'www' in subdomains


WWWPresent = CachedCallableFeature(name='www-present',
                                   return_type=FeatureReturnType.INTEGER,
                                   eval_func=_www_present)
"""Evaluates whether one of the subdomains is 'www'.

This feature returns binary values in {0, 1}.
If a 'www' subdomain is present it returns 1, otherwise 0.

Notes
-----
This feature depends on :py:data:`.SafeSubdomainsSuffixSplit`.

Examples
--------
>>> WWWPresent.evaluate('ieee-security.org')
False

"""
