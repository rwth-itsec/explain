from __future__ import annotations

from typing import Iterable, List, Union

import numpy as np
import scipy.sparse as sp
from joblib import Parallel, delayed
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.validation import check_is_fitted

from explain.base.features import CachedCallableFeature, Feature, FeatureCollection


class FeatureExtraction(BaseEstimator, TransformerMixin):
    """Estimator to extract a list of features.

    Parameters
    ----------
    features : iterable of :py:class:`.Feature` or :py:class:`.FeatureCollection`
        Features to extract.
    n_jobs : int, optional
        Number of parallel running processes extracting features.
    verbose : bool, optional
        Verbosity level of the extraction execution.

    """
    DUMMY_DOMAIN = 'example.edu'

    @staticmethod
    def _check_feature_collections(collections):
        flattened_features = []

        for collection in collections:
            if hasattr(collection, 'unpack'):
                flattened_features.extend(collection.unpack())
            else:
                flattened_features.append(collection)

        return flattened_features

    def __init__(self,
                 features: Iterable[Union[Feature, FeatureCollection]],
                 n_jobs: int = 1,
                 verbose: bool = False):
        self.features = features
        self.n_jobs = n_jobs
        self.verbose = verbose

    def _extract_single_sample(self, sample):
        return [feature.evaluate(sample) for feature in self.features_]

    def fit(self,
            X,
            y=None):
        """Fit estimator.

        Notes
        -----
        This function clears the lru_cache of :py:meth:`.CachedCallableFeature.evaluate`.

        """
        # Unpack collection of features with similar 'base' function.
        features = self._check_feature_collections(self.features)

        # detect dependencies by inspecting cache
        estimated_dependencies = []
        for feature in features:
            if not isinstance(feature, CachedCallableFeature):
                # just prepend non cached features
                estimated_dependencies.append((feature, np.inf))
                continue

            # TODO: dirty, better solution? or at least notify users that this clears the cache of the evaluate function
            CachedCallableFeature.evaluate.cache_clear()
            feature.evaluate(self.DUMMY_DOMAIN)
            estimated = CachedCallableFeature.evaluate.cache_info().misses - 1
            CachedCallableFeature.evaluate.cache_clear()

            estimated_dependencies.append((feature, estimated))

        self.features_ = [feature for feature, _ in sorted(estimated_dependencies, key=lambda p: p[1])]
        return self

    def transform(self, X):
        """Extract features.

        Parameters
        ----------
        X : list
            Samples.

        Returns
        -------
        X_extracted : sparse matrix, shape (n_samples, n_features)
            Feature matrix.

        Notes
        -----
        This function forces the `loky` backend from :doc:`joblib <joblib:parallel>` to enable the usage of cached `evaluate` calls.

        """
        check_is_fitted(self, ['features_'])

        if self.n_jobs == 1:
            Xs = [self._extract_single_sample(sample)
                  for sample
                  in X]
        else:
            # force loky backend to prevent sharing of memory (and therefore lru_cache)
            Xs = Parallel(backend="loky",
                          n_jobs=self.n_jobs,
                          verbose=self.verbose)(delayed(self._extract_single_sample)(sample)
                                                for sample
                                                in X)
        return sp.csr_matrix(np.array(Xs))

    def get_feature_names(self) -> List[str]:
        check_is_fitted(self, ['features_'])

        return [feature.name for feature in self.features_]
