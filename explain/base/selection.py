import timeit
from typing import Iterable

import numpy as np
import pandas as pd
import sklearn
from sklearn.base import MetaEstimatorMixin, ClassifierMixin, BaseEstimator, clone
from sklearn.feature_selection import RFE, RFECV
from sklearn.inspection import permutation_importance
from sklearn.metrics import check_scoring
from sklearn.model_selection import train_test_split
from sklearn.utils.multiclass import type_of_target
from sklearn.utils.sparsefuncs import mean_variance_axis
from skrebate import ReliefF, MultiSURF

from explain.base import FeatureExtraction
from explain.base.features import Feature, FeatureReturnType

__all__ = [
    'check_discrete_features',

    'anova_f',
    'mutual_information_classif',
    'permutation_importance_score',
    'relief_score',
    'rfe_rank',
    'target_correlation',
    'variance',

    'FixedMultiSURF',
    'FixedReliefF',
    'PermutationImportanceClassifier',
]


def _extract_features(X,
                      *,
                      features,
                      perform_extraction,
                      n_jobs,
                      verbose):
    if not perform_extraction:
        return X

    fe = FeatureExtraction(features=features, n_jobs=n_jobs, verbose=verbose)
    return fe.fit(X).transform(X)


def check_discrete_features(X,
                            *,
                            discrete_features,
                            features: Iterable[Feature] = None):
    if discrete_features == 'determine':
        if features is None:
            continuous_mask = np.apply_along_axis(lambda feature_col: type_of_target(feature_col)
                                                                      in ['continuous', 'continuous-multioutput'],
                                                  axis=0,
                                                  arr=X.todense() if hasattr(X, 'todense') else X)
            return ~continuous_mask

        # TODO: throw error if SPECIAL feature type is in features?
        return np.array([feature.return_type == FeatureReturnType.INTEGER for feature in features])

    return discrete_features


def mutual_information_classif(X, y,
                               *,
                               discrete_features='determine',
                               n_neighbors=3,
                               copy=True,
                               features: Iterable[Feature] = None,
                               perform_extraction=False,
                               n_jobs=1,
                               verbose=False,
                               random_state=None):
    """Estimate mutual information of features and discrete target labels.

    Parameters
    ----------
    X : array-like or sparse matrix, shape (n_samples,)
        Samples.
    y : array-like
        Target labels of the samples.
    discrete_features : {'determine', 'auto', bool, array-like}, default='determine'
        If 'determine', evaluates the return type ('discrete' or 'continuous') on a per feature basis.
    features : None or iterable of :py:class:`.Feature`
        Features to extract if perform_extraction = True.
    perform_extraction : bool
        If true, assumes features ≠ None and performs feature extraction on X.

    """
    X = _extract_features(X,
                          features=features,
                          perform_extraction=perform_extraction,
                          n_jobs=n_jobs,
                          verbose=verbose)
    discrete_features = check_discrete_features(X,
                                                discrete_features=discrete_features,
                                                features=features)

    # TODO: is there another workaround?
    if hasattr(X, 'todense'):
        X = X.todense()

    return sklearn.feature_selection.mutual_info_classif(X, y,
                                                         discrete_features=discrete_features,
                                                         n_neighbors=n_neighbors,
                                                         copy=copy,
                                                         random_state=random_state)


def variance(X, y=None,
             *,
             normalize=None,
             features: Iterable[Feature] = None,
             perform_extraction=False,
             return_means=False,
             n_jobs=1,
             verbose=False):
    """Estimate variance of features.

    Parameters
    ----------
    X : array-like or sparse matrix, shape (n_samples,)
        Samples.
    y : array-like or None
        Target labels of the samples, will be ignored.
    normalize : {'dispersion', None}
        If 'dispersion', normalizes the variances by the mean feature values.
    features : None or iterable of :py:class:`.Feature`
        Features to extract if perform_extraction = True.
    perform_extraction : bool
        If true, assumes features ≠ None and performs feature extraction on X.
    return_means : bool, default=False
        If true, additionally returns tuple mean feature values.

    """
    X = _extract_features(X,
                          features=features,
                          perform_extraction=perform_extraction,
                          n_jobs=n_jobs,
                          verbose=verbose)
    if hasattr(X, 'todense'):
        means, variances = mean_variance_axis(X, axis=0)
    else:
        means, variances = np.nanmean(X, axis=0), np.nanvar(X, axis=0)

    if normalize == 'dispersion':
        variances /= means

    if return_means:
        return variances, means

    return variances


def anova_f(X, y,
            *,
            features: Iterable[Feature] = None,
            perform_extraction=False,
            n_jobs=1,
            verbose=False):
    """Calculate ANOVA F-value for the provided samples.

    Parameters
    ----------
    X : array-like or sparse matrix, shape (n_samples,)
        Samples.
    y : array-like
        Target labels of the samples.
    features : None or iterable of :py:class:`.Feature`
        Features to extract if perform_extraction = True.
    perform_extraction : bool
        If true, assumes features ≠ None and performs feature extraction on X.

    """
    X = _extract_features(X,
                          features=features,
                          perform_extraction=perform_extraction,
                          n_jobs=n_jobs,
                          verbose=verbose)
    # sklearn.feature_selection.f_classif(X, y)[0]
    return sklearn.feature_selection.f_classif(X, y)


def target_correlation(X, y,
                       *,
                       method='pearson',
                       features: Iterable[Feature] = None,
                       perform_extraction=False,
                       n_jobs=1,
                       verbose=False):
    """Calculate correlation between provided features and target labels.

    Parameters
    ----------
    X : array-like or sparse matrix, shape (n_samples,)
        Samples.
    y : array-like
        Target labels of the samples.
    method : {'pearson', 'kendall', 'spearman'} or Callable
        Method of correlation.
    features : None or iterable of :py:class:`.Feature`
        Features to extract if perform_extraction = True.
    perform_extraction : bool
        If true, assumes features ≠ None and performs feature extraction on X.

    """
    X = _extract_features(X,
                          features=features,
                          perform_extraction=perform_extraction,
                          n_jobs=n_jobs,
                          verbose=verbose)

    # TODO: does it have to be dense?
    if hasattr(X, 'todense'):
        X = X.todense()

    # TODO: easier / better method (readability)?
    df = pd.DataFrame(np.hstack([X, y.reshape(-1, 1)]))  # append target column
    return df.corr(method=method).to_numpy()[:-1, -1]  # last column contains correlation values (excluding last row)


# Adapted from skrebate's ReliefF._get_attribute_info to utilize provided discrete features mask.
def _skrebate_relief_attribute_info(algorithm):
    # manipulate function to use correct/override discrete/continuous mask
    attr = dict()
    X_t = algorithm._X.transpose()

    for i, is_discrete in enumerate(algorithm.discrete_features):
        header = algorithm._headers[i]
        feature_col = X_t[i]

        if algorithm._missing_data_count > 0:
            # TODO: handle sparse X
            # if hasattr(feature_col, 'todense'):
            # TODO: np.logical_not(np.isnan(feature_col)) should be == ~np.isnan(feature_col)
            feature_col = feature_col[np.logical_not(np.isnan(feature_col))]  # Exclude any missing values from consideration

        if is_discrete:
            attr[header] = ('discrete', 0, 0, 0, 0)
        else:
            mx = np.max(feature_col)
            mn = np.min(feature_col)
            sd = np.std(feature_col)
            attr[header] = ('continuous', mx, mn, mx - mn, sd)

    return attr


# inherit MultiSURF/ReliefF to adjust discrete/continuous selection to
# prevent skrebate from labeling the target values as continuous
class FixedMultiSURF(MultiSURF):
    """MultiSURF algorithm that allows to mark continuous/discrete features and target labels.

    """

    def _get_attribute_info(self):
        return _skrebate_relief_attribute_info(self)

    # TODO: Add n_features_to_select
    def __init__(self, discrete_features, verbose=False, n_jobs=1):
        super().__init__(verbose=verbose, n_jobs=n_jobs)
        self.discrete_features = discrete_features

    def fit(self,
            X,
            y):
        """Fit algorithm.

        Parameters
        ----------
        X : array-like
            Preprocessed samples.
        y : array-like
            Target labels.

        """
        num_classes = np.unique(y).size
        self.discrete_threshold = num_classes

        return super().fit(X,
                           y)


class FixedReliefF(ReliefF):
    """ReliefF algorithm that allows to mark continuous/discrete features and target labels.

    """
    def _get_attribute_info(self):
        return _skrebate_relief_attribute_info(self)

    # TODO: Add n_features_to_select
    def __init__(self, discrete_features, n_neighbors=100, verbose=False, n_jobs=1):
        super().__init__(n_neighbors=n_neighbors, verbose=verbose, n_jobs=n_jobs)
        self.discrete_features = discrete_features

    def fit(self,
            X,
            y):
        """Fit algorithm.

        Parameters
        ----------
        X : array-like
            Preprocessed samples.
        y : array-like
            Target labels.

        """
        num_classes = np.unique(y).size
        self.discrete_threshold = num_classes

        return super().fit(X,
                           y)


def relief_score(X, y,
                 *,
                 features: Iterable[Feature] = None,
                 discrete_features='determine',
                 method='relieff',
                 undersampling=None,
                 n_neighbors=100,
                 perform_extraction=False,
                 verbose=False,
                 n_jobs=None):
    """Estimate Relief score of features.

    Parameters
    ----------
    X : array-like or sparse matrix, shape (n_samples,)
        Samples.
    y : array-like
        Target labels of the samples.
    features : None or iterable of :py:class:`.Feature`
        Features to extract if perform_extraction = True.
    discrete_features : {'determine', 'auto', bool, array-like}, default='determine'
        If 'determine', evaluates the return type ('discrete' or 'continuous') on a per feature basis.
    method : {'relieff', 'multisurf'}
        Relief algorithm.
    undersampling : :py:class:`.RandomUndersampling` or None
        If not None, performs random undersampling of the provided samples.
    perform_extraction : bool
        If true, assumes features ≠ None and performs feature extraction on X.

    """
    X = _extract_features(X,
                          features=features,
                          perform_extraction=perform_extraction,
                          n_jobs=n_jobs,
                          verbose=verbose)

    n_features = X.shape[1]

    # assume that integer values ~= discrete
    discrete_features = check_discrete_features(X,
                                                discrete_features=discrete_features,
                                                features=features)

    if undersampling is not None:
        X, y = undersampling.fit_resample(X, y)

    # ignore constant features as skrebate's algorithm propagates resulting NaN calculations
    # TODO: notify authors (maybe just use sklearn's MinMaxScaler?)
    var = variance(X,
                   y,
                   features=features,
                   perform_extraction=False,
                   n_jobs=n_jobs)

    # TODO: numerical precision problems?
    constant_features = var == 0.

    X = X[:, ~constant_features]
    discrete_features = discrete_features[~constant_features]

    if method == 'relieff':
        algorithm = FixedReliefF(discrete_features=discrete_features,
                                 n_neighbors=n_neighbors,
                                 verbose=verbose,
                                 n_jobs=n_jobs)
    elif method == 'multisurf':
        algorithm = FixedMultiSURF(discrete_features=discrete_features,
                                   verbose=verbose,
                                   n_jobs=n_jobs)
    else:
        raise ValueError(f'Unknown Relief scoring algorithm {method}.')

    if hasattr(X, 'toarray'):
        # skrebate doesn't support dense matrices
        X = X.toarray()

    algorithm.fit(X, y)

    scores = np.empty(shape=n_features)
    scores[constant_features] = -np.inf
    scores[~constant_features] = algorithm.feature_importances_

    return scores


class PermutationImportanceClassifier(MetaEstimatorMixin, ClassifierMixin, BaseEstimator):
    """Classifier using permutation importance to calculate feature importances.

    Parameters
    ----------
    estimator : BaseEstimator
        Wrapped estimator used in permutation importance calculation.
    holdout_size : float or int
        Holdout size to determine set to use in permutation importance calculation.
    scoring : str or callable
        Scorer to use.
    n_repeats : int
        Number of times to permute a feature.

    """
    def __init__(self, estimator,
                 *,
                 holdout_size,
                 scoring=None,
                 n_repeats=5,
                 n_jobs=None,
                 verbose=False):
        self.estimator = estimator
        self.holdout_size = holdout_size
        self.scoring = scoring
        self.n_repeats = n_repeats
        self.n_jobs = n_jobs
        self.verbose = verbose

    # TODO: Sanitize input?
    def fit(self,
            X,
            y):
        X_train, X_holdout, y_train, y_holdout = train_test_split(X, y, test_size=self.holdout_size, stratify=y)

        if self.verbose:
            print(f'Train estimator on {X_train.shape[0]} samples.')
            start_time = timeit.default_timer()

        scoring = check_scoring(self.estimator, self.scoring)

        self.estimator_ = clone(self.estimator)
        self.estimator_.fit(X_train,
                            y_train)

        if self.verbose:
            print(f'Trained estimator in {timeit.default_timer() - start_time}s.')
            print(f'Calculating permutation importance of {X_holdout.shape[1]} features on {X_holdout.shape[0]} samples.')
            start_time = timeit.default_timer()

        self.feature_importances_ = permutation_importance(self.estimator_,
                                                           X_holdout.todense(),
                                                           y_holdout,
                                                           scoring=scoring,
                                                           n_repeats=self.n_repeats,
                                                           n_jobs=self.n_jobs).importances_mean

        if self.verbose:
            print(f'Calculated permutation importance in {timeit.default_timer() - start_time}s.')

        return self

    def predict(self, X):
        return self.estimator_.predict(X)


def rfe_rank(X, y,
             *,
             estimator,
             metric='mdi',
             holdout_size=0.1,
             cv=5,
             features: Iterable[Feature] = None,
             perform_extraction=False,
             n_jobs=None,
             verbose=False):
    """Estimate RFE(CV) ranking of features using Forest Classifiers.

    Parameters
    ----------
    X : array-like or sparse matrix, shape (n_samples,)
        Samples.
    y : array-like
        Target labels of the samples.
    estimator : BaseEstimator
        Estimator used in RFE.
    metric : {'mdi', 'mda', 'pi'}
        If 'mdi', uses 'Mean Decrease in Impurity' as feature importance.
        If 'mda' or 'pi', uses 'Permutation Importance' as feature importance.
    holdout_size : float or int, default=0.1
        Holdout size to determine set to use in permutation importance calculation.
    cv : {'disable', int, cross-validation generator or an iterable}, default=5
        Cross-validation strategy used in cross validated RFE algorithm.
        If 'disable', uses non cross validated RFE algorithm.
    features : None or iterable of :py:class:`.Feature`
        Features to extract if perform_extraction = True.
    perform_extraction : bool
        If true, assumes features ≠ None and performs feature extraction on X.

    """
    X = _extract_features(X,
                          features=features,
                          perform_extraction=perform_extraction,
                          n_jobs=n_jobs,
                          verbose=verbose)

    if metric is None:
        rfe_estimator = estimator

    if isinstance(metric, str):
        if metric == 'mdi':
            # sklearn's builtin decision tree metric is mdi
            rfe_estimator = estimator
        elif metric in ['mda', 'pi']:
            rfe_estimator = PermutationImportanceClassifier(estimator,
                                                            holdout_size=holdout_size,
                                                            scoring='f1_macro',
                                                            n_jobs=n_jobs,
                                                            verbose=verbose)
        else:
            raise ValueError(f'Unknown feature importance metric {metric}.')

    if cv == 'disable':
        # noinspection PyUnboundLocalVariable
        rfe = RFE(rfe_estimator,
                  n_features_to_select=1)
    else:
        # noinspection PyUnboundLocalVariable
        rfe = RFECV(rfe_estimator,
                    cv=cv,
                    scoring='f1_macro')
    rfe.fit(X, y)
    return rfe.ranking_


def permutation_importance_score(X, y,
                                 *,
                                 estimator,
                                 holdout_size=0.1,
                                 n_repeats=5,
                                 features: Iterable[Feature] = None,
                                 perform_extraction=False,
                                 n_jobs=None,
                                 verbose=False):
    """Estimate permutation importance of provided samples.

    Parameters
    ----------
    X : array-like or sparse matrix, shape (n_samples,)
        Samples.
    y : array-like
        Target labels of the samples.
    estimator : BaseEstimator
        Estimator used to calculate permutation importance.
    holdout_size : float or int, default=0.1
        Holdout size to determine set to use in permutation importance calculation.
    n_repeats : int
        Number of times to permute a feature.
    features : None or iterable of :py:class:`.Feature`
        Features to extract if perform_extraction = True.
    perform_extraction : bool
        If true, assumes features ≠ None and performs feature extraction on X.

    """
    X = _extract_features(X,
                          features=features,
                          perform_extraction=perform_extraction,
                          n_jobs=n_jobs,
                          verbose=verbose)

    permutation_classifier = PermutationImportanceClassifier(estimator,
                                                             holdout_size=holdout_size,
                                                             scoring='f1_macro',
                                                             n_repeats=n_repeats,
                                                             n_jobs=n_jobs,
                                                             verbose=verbose)

    permutation_classifier.fit(X, y)
    return permutation_classifier.feature_importances_
