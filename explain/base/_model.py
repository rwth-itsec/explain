from abc import ABC, abstractmethod

from sklearn.base import BaseEstimator, ClassifierMixin, MetaEstimatorMixin
from sklearn.utils.metaestimators import if_delegate_has_method
from sklearn.utils.validation import check_is_fitted


class BaseModel(ABC, BaseEstimator, ClassifierMixin, MetaEstimatorMixin):
    """Base class for all models in EXPLAIN.
    """

    @abstractmethod
    def preprocess(self, X, y):
        """Execute preprocessing steps performed before training and classification.

        Parameters
        ----------
        X : array-like
            Unprocessed samples.
        y : array-like
            Target labels of the samples.

        Returns
        -------
        X_preprocessed : array, shape (n_samples, n_features)
            Preprocessed samples.

        """

    @abstractmethod
    def fit(self, X, y):
        """Fit model.
        """

    @abstractmethod
    def predict(self, X):
        """Predict using this model.

        Parameters
        ----------
        X : array-like or sparse matrix, shape (n_samples, n_features)
            Preprocessed samples.

        Returns
        -------
        y_pred : array, shape (n_samples,)
            Predicted target labels.

        """

    @if_delegate_has_method(delegate='estimator_')
    def predict_proba(self, X):
        check_is_fitted(self)
        return self.estimator_.predict_proba(X)

    @if_delegate_has_method(delegate='estimator_')
    def decision_function(self, X):
        check_is_fitted(self)
        return self.estimator_.decision_function(X)
