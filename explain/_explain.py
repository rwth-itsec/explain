import json

from sklearn.multiclass import OneVsOneClassifier, OneVsRestClassifier

from explain.base import BaseModel, WeightedRandomForestClassifier, FeatureExtraction, CostSensitivity
from explain.base.features.examples import ALL as ALL_EXAMPLE_FEATURES
from explain.base.features.utility import features_from_str_list


class Explain(BaseModel):
    def __init__(self,
                 n_estimators,
                 *,
                 bootstrap,
                 class_weight,
                 criterion,
                 feature_set,
                 max_features,
                 max_depth,
                 multiclass=None,
                 n_jobs=1,
                 verbose=False):
        self.bootstrap = bootstrap
        self.class_weight = class_weight
        self.criterion = criterion
        self.feature_set = feature_set
        self.max_features = max_features
        self.max_depth = max_depth
        self.multiclass = multiclass
        self.n_estimators = n_estimators
        self.n_jobs = n_jobs
        self.verbose = verbose

    def preprocess(self, X, y):
        extraction = FeatureExtraction(features=self.feature_set,
                                       n_jobs=self.n_jobs,
                                       verbose=self.verbose)

        return extraction.fit(X).transform(X)

    def fit(self, X, y):
        self.estimator_ = WeightedRandomForestClassifier(n_estimators=self.n_estimators,
                                                         bootstrap=self.bootstrap,
                                                         class_weight=self.class_weight,
                                                         criterion=self.criterion,
                                                         max_features=self.max_features,
                                                         max_depth=self.max_depth,
                                                         n_jobs=self.n_jobs,
                                                         verbose=self.verbose > 50)

        if self.multiclass is not None:
            if self.multiclass == 'ovr':
                self.estimator_ = OneVsRestClassifier(self.estimator_)
            elif self.multiclass == 'ovo':
                self.estimator_ = OneVsOneClassifier(self.estimator_)
            else:
                raise ValueError(f'Invalid multiclass argument {self.multiclass}.')

        return self.estimator_.fit(X, y)

    def predict(self, X):
        return self.estimator_.predict(X)

    @classmethod
    def from_json(cls, file_path, *, n_jobs=1, verbose=False):
        def sensitivity_hook(dct):
            if 'sensitivity' in dct:
                return CostSensitivity(dct['sensitivity'])
            return dct

        with open(file_path) as json_file:
            explain_config = json.load(json_file, object_hook=sensitivity_hook)
        feature_set = features_from_str_list(explain_config['feature_set'], ALL_EXAMPLE_FEATURES)

        return Explain(n_estimators=explain_config['n_estimators'],
                       bootstrap=explain_config['bootstrap'],
                       class_weight=explain_config['class_weight'],
                       criterion=explain_config['criterion'],
                       feature_set=feature_set,
                       max_features=explain_config['max_features'],
                       max_depth=explain_config['max_depth'],
                       multiclass=explain_config['multiclass'],
                       n_jobs=n_jobs,
                       verbose=verbose)
