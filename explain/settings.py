import os

from explain.misc.data import load_public_suffix_list

__all__ = [
    'LRU_CACHE_SIZE',
    'PUBLIC_SUFFIX_LIST_SOURCE',
    'TLD_LIST_SOURCE',

    'VALID_SUFFIXES',
    'VALID_TLDS'
]

_SETTINGS_FILE_ROOT = os.path.dirname(os.path.abspath(__file__))

PUBLIC_SUFFIX_LIST_SOURCE = os.getenv('PUBLIC_SUFFIX_LIST_SOURCE',
                                      os.path.join(_SETTINGS_FILE_ROOT, '..', 'data', 'public_suffix_list.dat'))
TLD_LIST_SOURCE = os.getenv('TLD_LIST_SOURCE', os.path.join(_SETTINGS_FILE_ROOT, '..', 'data', 'tld.dat'))

LRU_CACHE_SIZE = int(os.getenv('LRU_CACHE_SIZE', 136))


VALID_SUFFIXES = load_public_suffix_list(PUBLIC_SUFFIX_LIST_SOURCE, comments=['!', '//'])

# IANA uses a similar style as https://publicsuffix.org/ (c.f. https://data.iana.org/TLD/tlds-alpha-by-domain.txt)
VALID_TLDS = load_public_suffix_list(TLD_LIST_SOURCE, comments=['#'])
