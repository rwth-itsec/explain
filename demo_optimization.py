import argparse
import os
import pickle

from sklearn.model_selection import RandomizedSearchCV, StratifiedKFold
from sklearn.multiclass import OneVsRestClassifier
from sklearn.pipeline import Pipeline

from explain.base import WeightedRandomForestClassifier, CostSensitivity, SimpleDelegatedEstimator, \
    FeatureExtraction
from explain.base.features.examples import ALL as ALL_EXAMPLE_FEATURES
from explain.base.features.utility import features_from_str_list
from explain.misc.data import CompatibilityDataSet
from explain.misc.sampling import RandomUndersampling

PARAM_GRID = {
    'clf': [
        SimpleDelegatedEstimator(WeightedRandomForestClassifier(n_jobs=-1)),
        OneVsRestClassifier(WeightedRandomForestClassifier(n_jobs=-1))
    ],
    'clf__estimator__bootstrap': [True, False],
    'clf__estimator__class_weight': [None, 'balanced', 'balanced_subsample', CostSensitivity(0.1), CostSensitivity(0.3),
                                     CostSensitivity(0.5)],
    'clf__estimator__criterion': ['gini', 'entropy'],
    'clf__estimator__max_depth': [None, 10, 20, 30, 40, 50],
    'clf__estimator__max_features': [None, 0.25, 0.5, 0.75, 'sqrt', 'log2'],
    'clf__estimator__n_estimators': [50, 100, 200, 300, 400, 500]
}

if __name__ == "__main__":
    # TODO: update features parameter to accept a file path to a config

    parser = argparse.ArgumentParser(description='Demo execution of Explain\'s hyperparameter optimization.')
    parser.add_argument('--dataset', type=str, required=True)
    parser.add_argument('--features', type=str, required=False, default=None)
    parser.add_argument('--n_jobs', type=int, required=False, default=-1)  # only for potential feature extraction
    parser.add_argument('--verbose', type=int, required=False, default=False)

    # destination folder
    parser.add_argument('--destination', type=str, required=False, default='')

    # use to get an initial idea for possible distribution
    # performs random undersampling for classes with more than N samples
    parser.add_argument('--undersample', type=int, required=False, default=10000)

    args = parser.parse_args()

    dataset = CompatibilityDataSet.load(args.dataset)
    X, y = dataset.expand()

    if args.undersample > 0:
        X, y = RandomUndersampling(samples_per_class=args.undersample).fit_resample(X, y)

    n_jobs = args.n_jobs
    verbose = args.verbose

    features = args.features
    if features is not None:
        # expect X to NOT be extracted
        with open(features, encoding='utf-8') as feature_set_file:
            selected_features = features_from_str_list(feature_set_file, ALL_EXAMPLE_FEATURES)
        feature_extraction = FeatureExtraction(selected_features, n_jobs=n_jobs, verbose=verbose)
        X = feature_extraction.fit(X).transform(X)

    optimization = RandomizedSearchCV(
        Pipeline([
            ('clf', None)  # placeholder
        ]),
        param_distributions=PARAM_GRID,
        n_iter=120,
        scoring='f1_macro',
        cv=StratifiedKFold(n_splits=5),
        refit=False,
        verbose=verbose
    )

    optimization.fit(X, y)

    with open(os.path.join(args.destination, 'hyperparameter_optimization.pkl'), 'wb') as optimization_result_file:
        pickle.dump(optimization.cv_results_, optimization_result_file)
