# First Step Towards EXPLAINable DGA Multiclass Classification

This repository contains the source code of EXPLAIN, a classification system and library using random forests to perform multiclass classification of malware families that utilize domain generation algorithms (DGAs).

EXPLAIN is presented in the paper _First Step Towards EXPLAINable DGA Multiclass Classification_<sup>[[1]](#1)</sup>.
The version used for the paper can be found in the branch `paper`, while the `master` branch tracks a revised version with improvements and bugfixes.

### _**Note that the following information refers to the latest version if not stated otherwise.**_

## Requirements
- Python 3.8 or higher
- Conda 4.8.5 or higher
- Use `conda env create -f environment.yml` to create a conda environment with all dependencies
- Activate the environment using `conda activate explain` (use `conda activate explain_paper` for the `paper` version)

## Documentation
Please refer to either the source code or _[Overview over EXPLAIN’s functions and data types](https://rwth-itsec.gitlab.io/explain/)_ for the documentation of the implementation.

## References
<a id="1">[1]</a> Arthur Drichel, Nils Faerber, and Ulrike Meyer. 2021. First Step Towards EXPLAINable DGA Multiclass Classification. In _The 16th International Conference on Availability, Reliability and Security (ARES 2021), August 17–20, 2021, Vienna, Austria_. ACM, New York, NY, USA, 13 pages. https://doi.org/10.1145/3465481.3465749

## Acknowledgments
This project has received funding from the European Union’s Horizon 2020 research and innovation programme under grant agreement No 833418.
