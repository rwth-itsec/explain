explain.misc.evaluation
=======================

.. automodule:: explain.misc.evaluation

.. rubric:: Classes

.. autosummary::
    :template: simple_class.rst
    :toctree: _generated

    .. currentmodule:: explain.misc.evaluation
    Evaluation