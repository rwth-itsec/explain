explain.misc.sampling
=====================

.. automodule:: explain.misc.sampling

.. rubric:: Classes

.. autosummary::
    :template: simple_class.rst
    :toctree: _generated

    .. currentmodule:: explain.misc.sampling
    RandomUndersampling