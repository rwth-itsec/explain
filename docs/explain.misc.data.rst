explain.misc.data
=================

.. automodule:: explain.misc.data
.. currentmodule:: explain.misc.data

.. rubric:: Functions
.. autosummary::
    :toctree: _generated

    load_public_suffix_list

.. rubric:: Classes
.. autosummary::
    :template: simple_class.rst
    :toctree: _generated

    .. currentmodule:: explain.misc.data
    CompatibilityDataSet