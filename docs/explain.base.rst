explain.base
============

.. automodule:: explain.base

.. rubric:: Modules

.. toctree::
   :maxdepth: 1
   
   explain.base.features
   explain.base.selection   

.. rubric:: Classes

.. autosummary::
    :template: simple_estimator.rst
    :toctree: _generated

    .. currentmodule:: explain.base
    BaseModel
    FeatureExtraction