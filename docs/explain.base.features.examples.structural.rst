explain.base.features.examples.structural
=========================================

.. automodule:: explain.base.features.examples.structural

.. rubric:: Features
.. autosummary::
    :toctree: _generated

    .. currentmodule:: explain.base.features.examples.structural
    ContainsIPv4
    DigitExclusiveSubdomainsRatio
    DomainNameLength
    HexExclusiveSubdomainsRatio
    PrefixRepetitionExclusive
    SingleCharacterSubdomain
    SecondLevelLength
    SecondLevelRepeatedPrefix
    SubdomainsContainSuffix
    SubdomainsDNSLevel
    SubdomainsLength
    SubdomainsMeanLength
    SuffixDNSLevel
    SuffixLength
    TLDInSubdomains
    ValidTLD
    WWWPresent