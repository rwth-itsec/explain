explain.base.features.examples.linguistic
=========================================

.. automodule:: explain.base.features.examples.linguistic

.. rubric:: Features

.. autosummary::
    :toctree: _generated
	
    .. currentmodule:: explain.base.features.examples.linguistic
    AdjacentDuplicatesRatio
    AlphabetFeatures
    CharacterRatioFeatures
    ConsecutiveCharactersRatios
    ConsonantToVowelRatio
    ContainsDigits
    DigitSums
    FirstCharacterPair
    FirstDigitDistances
    InverseHammingDistance
    MaxStreakLengthFeatures
    RepeatedCharactersRatio
    SuffixStandardDeviation
    SyllableCount
    WeightedStreaks