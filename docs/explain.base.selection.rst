explain.base.selection
======================

.. automodule:: explain.base.selection
.. currentmodule:: explain.base.selection

This module contains various wrappers and functions to extract selection metrics of features.

.. rubric:: Functions

.. autosummary::
    :toctree: _generated
    
    anova_f
    mutual_information_classif
    permutation_importance_score
    relief_score
    rfe_rank
    target_correlation
    variance

.. rubric:: Classes

.. autosummary::
    :template: simple_estimator.rst
    :toctree: _generated

    FixedMultiSURF
    FixedReliefF
    PermutationImportanceClassifier