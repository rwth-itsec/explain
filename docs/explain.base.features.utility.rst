explain.base.features.utility
=============================

.. automodule:: explain.base.features.utility
.. currentmodule:: explain.base.features.utility

.. rubric:: Functions
.. autosummary::
	:toctree: _generated

	features_from_str_list
	safe_subdomains_suffix_split

.. rubric:: Features
.. autosummary::
	:toctree: _generated

	SafeSubdomainsSuffixSplit