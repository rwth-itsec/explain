explain.base.features
=====================

.. automodule:: explain.base.features
.. currentmodule:: explain.base.features

.. rubric:: Modules

.. toctree::
   :maxdepth: 1
   
   Example Linguistic Features <explain.base.features.examples.linguistic>
   Example Statistical Features <explain.base.features.examples.statistical>
   Example Structural Features <explain.base.features.examples.structural>
   Example Utility Features <explain.base.features.examples.utility>
   Utility Functions and Features <explain.base.features.utility>

.. rubric:: Classes
.. autosummary::
    :template: simple_class.rst
    :toctree: _generated
    
    CachedCallableFeature
    Feature
    FeatureCollection

.. rubric:: Enums
.. autosummary::
    :template: simple_enum.rst
    :toctree: _generated

    FeatureReturnType